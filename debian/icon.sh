#!/bin/bash
# create icon for debian menu system

rm -f defendguin-icon.bmp
cp ../data/images/ship/ship-right2.bmp defendguin.bmp
mogrify -format xpm -geometry 32x32 \
	-map /usr/X11R6/include/X11/pixmaps/cmap.xpm \
	defendguin-icon.bmp
rm -f defendguin-icon.bmp
