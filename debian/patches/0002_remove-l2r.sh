Description: do not ship l2r.sh script
Author: Christian T. Steigies <cts@debian.org>

--- a/data/images/l2r.sh
+++ /dev/null
@@ -1,10 +0,0 @@
-#!/bin/sh
-
-echo Converting left tux images to right tux images...
-
-for i in 0 1 2 3 4 5 6 7
-do
-  echo $i
-  bmptoppm tux/l${i}.bmp | pnmflip -lr | ppmtobmp > tux/r${i}.bmp
-done
-  
\ No newline at end of file
